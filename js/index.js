if(typeof($.mask) !== "object") {
(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-zА-Яа-я]","*":"[A-Za-zА-Яа-я0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);
}

$(document).ready(function() {
    $('.toggle-menu').click(function(e) {
        e.preventDefault();

        $('.menu-wrapper').slideDown();
        $(this).slideUp();
        $('.close-menu').slideDown();
        $('body').addClass('opened-menu');
    });

    $(document).mouseup(function (e) {
        var div = $('.menu-wrapper');
        if ($('body').hasClass('opened-menu') && !$(e.target).hasClass('toggle-menu') && !div.is(e.target) && div.has(e.target).length === 0) {
            e.preventDefault();

            div.slideUp();
            $('.toggle-menu').slideDown();
            $('.close-menu').slideUp();
            $('body').removeClass('opened-menu');
        }
    });
    
    $('.form__checkbox').click(function() {
        var checked = $(this).find('input[type=checkbox]').prop('checked');
        
        if (checked) {
            $(this).addClass('form__checkbox_checked');
        } else {
            $(this).removeClass('form__checkbox_checked');
        }
    });
    
    $("body").on('click', '.scroll-to', function(e){
        $('html,body').stop().animate({ scrollTop: $(this.hash).offset().top}, 1000);
        e.preventDefault();
    });
    
    $('.mask-input').each(function() {
        var dataMask    = $(this).attr('data-mask');
        var placeholder = $(this).attr('data-placeholder');
        
        $(this).mask(dataMask !== undefined ? dataMask : '+7 (999) 999-99-99', {placeholder: (placeholder !== undefined ? placeholder : '_')});
    });
    
    if(typeof $.fn.owlCarousel !== 'undefined') {
        $('.slider__content').owlCarousel({
            autoWidth: true,
            margin: 11,
            center: false,
            onInitialized: function() {
                $('.owl-dot, .owl-prev, .owl-next, .slider__image').attr('tabindex', '-1');
            }
            //startPosition: Math.floor($('.slider__content').find('.slider__item').length / 2)
        });
    }
    
    if (typeof ymaps !== 'undefined') {
        ymaps.ready(function(){
            var contactsMap = new ymaps.Map('map', {
                center: [55.829451, 37.371218],
                zoom: 16
            });

            var myPlacemark = new ymaps.Placemark([55.829851, 37.371218] , {}, {
                iconLayout: 'default#image',
                iconImageHref: '/images/business_logo.png',
                iconImageSize: [48, 56],
                preset: 'islands#blueStretchyIcon'});  

            contactsMap.geoObjects.add(myPlacemark);
            contactsMap.behaviors.disable('drag');
        });
    }

    action();
    removablePlaceholder();
    freeConsultation();
    kit();
});

function action() {
    $('.action__show-content').click(function(e) {
        e.preventDefault();
        
        var activeClass = 'active';
        var $this       = $(this);
        var $action     = $this.closest('.action');
        var $content    = $action.find('.action__content');
        
        if ($action.hasClass(activeClass)) {
            $action.removeClass(activeClass);
            $content.hide();
        } else {
            $action.addClass(activeClass);
            $content.show();
            window.scrollTo({top: $content.offset().top - $this.height()});
        }
    });
}

function removablePlaceholder() {
    $(document).on('focus', '.removable-placeholder', function() {
        var $this       = $(this);
        var placeholder = $this.attr('placeholder');
        
        $this.attr('data-rplaceholder', placeholder !== undefined ? placeholder : $this.val());
        $this.val('').removeAttr('placeholder');
    });
    
    $(document).on('blur', '.removable-placeholder', function() {
        var $this = $(this);
        var placeholder = $this.attr('rplaceholder');

        $this.val($this.val().length > 0 ? $this.val() : placeholder);
    });
}

function freeConsultation() {
    $('.form__submit_invite').click(function(e) {
        e.preventDefault();
        
        var $button    = $(this);
        var $form      = $button.closest('.form_invite');
        var $input     = $form.find('.form__input_invite');
        var $preloader = $('#preloader');
        
        $preloader.show();
        
        if ($input.val().length > 0) {
            setTimeout(function() {
                var $wrapper = $input.parent();
                
                $button.remove();
                $input.remove();
                $('<p class="text-success">' + $form.attr('data-success-text') + '</p>').appendTo($wrapper);
                $preloader.hide();
            }, 1000);
        }
    });
}

function kit() {
    $('.form__input_kit')
        .focus(function() {
            var $this = $(this);

            $this.attr('data-val', $this.val()).val('');
        
            $this.closest('.kit__row').addClass('kit__row_hover');
        
            if ($this.attr('type') == 'checkbox') {
                $this.parent().addClass('form__checkbox_focus');
            }
        })
        .blur(function() {
            var $this = $(this);
        
            $this.closest('.kit__row').removeClass('kit__row_hover');

            if ($this.val().length == 0) {
                $this.val($this.attr('data-val'));
            }
        
            if ($this.attr('type') == 'checkbox') {
                $this.parent().removeClass('form__checkbox_focus');
            }
        })
        .keyup(function(e) {
            var $this  = $(this);
            var val    = $this.val();
            var code   = (e.keyCode ? e.keyCode : e.which);
            
            // Enter
            if (code == 13) {
                var num  = parseInt($this.attr('id').replace('kit_input_', ''));
                var next = $('#kit_input_' + (num + 1)).length > 0 ? (num + 1) : ($('#kit_input_0').length == 0 ? 1 : 0);

                $('#kit_input_' + next).focus();
            } else {
                if (val.match(/[^0-9]/g)) {
                    $this.val('');
                } else if (val.length > 1 && val.substr(0, 1) == 0) {
                    $this.val(val.substr(1))
                }
            }
        });
}